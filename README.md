  <a href="https://www.qworld.net">
    <img align="middle" src="qworld/images/QWorld.png" width="18%"/>
  </a>

## QWorld's QEC self-study module
The hardware of quantum computers is noisy, which prevents the execution of quantum algorithm. In order to reduce the impact of noise, quantum error correction is used to detect and correct errors. The Quantum Error Correction (QEC) self-study module is an introductory tutorial on quantum error correction. It first introduces classical error correction. It then helps the user explore simple quantum error correcting codes. This is followed by a discussion of stabilizer quantum error-corecting codes, which are both very simple and are the most commonly studied and used codes.

A python library called [Stac](https://github.com/abdullahkhalids/stac) has been developed to help the reader do common tasks with stabilizer codes.

## Prerequisite
Students should have completed [QBronze](https://qworld.net/workshop-bronze/) material or a similar course.

Some knowledge of group theory will help, but is not required.

## Content
Please refer to [Content](content.ipynb).

## Making Contributions 
Please create an issue for reporting typo or your corrections.

## License
The text and figures are licensed under the Creative Commons Attribution 4.0 International Public License (CC-BY-4.0), available at https://creativecommons.org/licenses/by/4.0/legalcode.

The code snippets in the notebooks are licensed under Apache License 2.0, available at http://www.apache.org/licenses/LICENSE-2.0.

## Credits
The contents of this repository are developed by [Abdullah Khalid](https://abdullahkhalid.com/).
